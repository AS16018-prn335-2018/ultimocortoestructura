#include<stdio.h>
#include<stdlib.h>

/**
 * Declaramos una estructura que define cada elemento
 * que componen nuestra pila
*/
struct nodo{
    int info;
    struct nodo *sig;
};


/**
 * declaramos un puntero que apunta al primer elemento
 * iniciandolo en nulo, cuando esta vacia la pila
*/
struct nodo *raiz=NULL;

void insertar(int x){//funcion para crear nodo en nuestra pila, y al mismo tiempo ingresar los datos, recibiendo de parametro un entero
    
    struct nodo *nuevo;
    nuevo = malloc(sizeof (struct nodo));//Hacemos uso de malloc por trabajar memoria dinamica
    nuevo->info = x;
    
    if (raiz == NULL) {//validamos que la raiz sea nula
        raiz=nuevo;
        nuevo->sig=NULL;
    }else
    {
        nuevo->sig = raiz;//decimos que ahora el dato "raiz" sera el siguiente que extraeremos despues del que acabamos de ingresar
        raiz=nuevo;//el dato ingresado pasa a ser raiz, y a estar en la cima de la pila
        
    }   
    
}
void imprimir(){//funcion para imprimir datos de la pila
    int contarNodos=0; //decalaramos una variable de tipo entero, para usarla como contador
    struct nodo *reco=raiz;
    printf("Lista Completa. \n");
    
    while(reco!=NULL){//esto mientras la pila aun tenga datos que mostrar
        printf("%i ", reco->info);//imprimimos la pila
        contarNodos++;//mientras el bucle siga, aumentamos en uno el contador
        reco=reco->sig;
        
    }
    printf("\n");
    printf("\nLa cantidad de nodos es: %d\n\n", contarNodos);//se imprime la cantidad de nodos
}
int extraer(){//funcion para extraer el dato deseado
    if(raiz != NULL){
        int informacion = raiz->info;
        struct nodo *bor =raiz;
        raiz=raiz->sig;
        free(bor);//liberamos de la memoria
        return informacion;
    }
    else{
        return -1;
    }

}
void liberar(){//funcion para eliminar dato de memoria al igual que el nodo
    
    struct nodo *reco = raiz;
    struct nodo *bor;
    while(reco != NULL){
        bor =reco;
        reco = reco->sig;
        free(bor);//liberamos de la memoria
    }
    
}
int main(void){
  //Ingresamos datos a la pila atravez de la funcion insertar  
  insertar(10);
  insertar(40);
  insertar(3);
  //Imprimimos los datos de la pila atravez de la funcion imprimir  
  imprimir();
  printf("extraemos de la pila:%i\n", extraer());
  imprimir();
  //elimininamos el dato extraido, al igual que el nodo
  liberar();
}
